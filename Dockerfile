ARG ANSIBLE_VERSION
ARG PYTHON_VERSION
ARG OPENSSH_CLIENT_VERSION=1:9.2p1-2+deb12u2

FROM python:${PYTHON_VERSION}-slim
ARG ANSIBLE_VERSION
ARG OPENSSH_CLIENT_VERSION
WORKDIR /root
RUN apt update -y \
    && apt install -y openssh-client=${OPENSSH_CLIENT_VERSION} \
    && pip install --no-cache-dir ansible==${ANSIBLE_VERSION}
COPY ansible.cfg /etc/ansible/ansible.cfg
